package com.mycompany.app;

import java.util.*;
import java.io.*;

/*
 * Album
 * 
 * version: 0.1
 */
/**
 *
 * @author Daniel Buzderewicz
 */
import java.util.Scanner;

public class AlbumClass extends Songs implements AlbumInterface {  

    String albumName;
    String artist;
    int releaseDate;
    List<String> listOfAlbums = new ArrayList<String>();
    List<String> listOfArtists = new ArrayList<String>();
    List<Integer> listOfReleaseDates = new ArrayList<Integer>();
    Map hashMap = new HashMap();

    public AlbumClass(String albumName) {
    }

    @Override

    public void newAlbum() {
    }

    @Override

    public void setAlbumName() {

        System.out.println("Album name: ");
        Scanner albumNameIn = new Scanner(System.in);
        albumName = albumNameIn.next();
        listOfAlbums.add(albumName);   
    }

    @Override
    public void setArtist() {

        System.out.println("Artist name: ");
        Scanner artistIn = new Scanner(System.in);
        artist = artistIn.next();
        listOfArtists.add(artist);

    }

    @Override
    public void setReleaseDate() throws ValidReleaseDate {

        System.out.println("Release date: ");
        Scanner releaseDateIn = new Scanner(System.in);

        releaseDate = releaseDateIn.nextInt();
        if (Integer.valueOf(releaseDate) > 2017) {

            hashMap.put(">2017", releaseDate);
            throw new ValidReleaseDate();     
        }
        listOfReleaseDates.add(releaseDate); 

    }

    public void getInfo() {

        System.out.println(albumName);
        System.out.println(artist);
        System.out.println(releaseDate);
    }

    public void getInfo(String typeOfInfo) {    

        if (typeOfInfo.equals("album")) {
            System.out.println(albumName);
        } else if (typeOfInfo.equals("artist")) {
            System.out.println(artist);
        } else if (typeOfInfo.equals("releaseDate")) {
            System.out.println(releaseDate);
        } else {
            System.out.println("You have given wrong type of info");
        }
    }

}
