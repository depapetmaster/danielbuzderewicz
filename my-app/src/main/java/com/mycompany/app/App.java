package com.mycompany.app;

import java.util.*;

/**
 * Main Class
 *
 * Author: Daniel Buzderewicz version: 0.1
 *
 */
public class App {

    public static void main(String[] args) {

        AlbumInterface album1 = new AlbumClass("steven");

        album1.setAlbumName();
        album1.setArtist();

        try {

            album1.setReleaseDate();

        } catch (ValidReleaseDate e) {

            System.out.println(e.getMessage());
            System.exit(0);
        }

        album1.getInfo();

        AlbumClass[] Names = new AlbumClass[10];
        Names[0] = new AlbumClass("dupek");
    }
}
