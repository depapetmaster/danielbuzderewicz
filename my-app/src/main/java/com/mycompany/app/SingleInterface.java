/*
 * 
 */
package com.mycompany.app;

/**
 *
 * @author Daniel Buzderewicz
 */
public interface SingleInterface extends AlbumInterface {

    @Override
    public void newAlbum();

    public void setAlbumName();

    public void setArtist();

    public void setReleaseDate() throws ValidReleaseDate;

    public void getInfo();

}
