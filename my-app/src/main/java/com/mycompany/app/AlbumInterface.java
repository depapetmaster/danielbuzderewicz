/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.app;

/**
 *
 * @author Admin
 */
public interface AlbumInterface {

    public void newAlbum();

    public void setAlbumName();

    public void setArtist();

    public void setReleaseDate() throws ValidReleaseDate;

    public void getInfo();

    public void getInfo(String s);
}
